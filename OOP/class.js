class Car {
  constructor(door, wheels) {
    this.door = door
    this.wheels = wheels
    this.obj = {
      price: 1231
    }
  }

  ngeGas(int) {
    for (let i = 0; i < int; i++) {
      console.log("Brummmm")
    }
  }
}

class PejabatCar extends Car {
  constructor(door, wheels, isStrobo) {
    super(door, wheels)
    this.isStrobo = isStrobo
  }

  mengosongkanJalan() {
    console.log("niu niu niu")
  }

  ngeGas(int) {
    // super.ngeGas(int)
    console.log(" Minggir woy" + int)
  }
}

class Bike {
  #BikePrivate
  constructor(type) {
    this.type = type
    this.#BikePrivate = "Priv bike"

  }

  gowes(int) {
    for (let i = 0; i < int; i++) {
      console.log("Brr")
    }
  }

  _protected() {
    console.log("ini protected");
  }
  static promosi() { // Static Method
    console.log("ini sepeda bagus")
  }
}

class sepedaMahal extends Bike {
  #private
  constructor(type, harga) {
    super(type)
    this.harga = harga
    this.#private = "Private property"
  }

  gowesLagi() {
    super.gowes(3)
    console.log(this.gowes())
    console.log(this.type)
  }

  gowes() {
    super.gowes(2)
    // console.log(name)
    // super.gowes(2)
    // console.log(int, name)
  }

  #OnlyHere() {
    console.log("Private function")
  }

  whosHere() {
    console.log(this.#private)
    // console.log(this.#BikePrivate);
    this.#OnlyHere()
  }

}

// Bike.prototype.gowes = (int) => {
//   console.log("Gowes" + int)
// }


const pajero = new Car(5, 4)
const lambo = new Car(2, 4)
const fortuner = new PejabatCar(4, 4, true)
const poligon = new Bike("Gunung")
const brompton = new sepedaMahal("brompton", 600000)

// poligon._protected()
// brompton.#OnlyHere() // ERROR Karena mengakses fungsi private
// brompton.whosHere()
// brompton.harga
// console.log(brompton.harga);
// console.log(brompton.#private);
//Over Ride

// pajero.ngeGas(3)
// fortuner.ngeGas()

//Over load
// brompton.gowes(3, "apa")

// fortuner.mengosongkanJalan()

// console.log(fortuner.ngeGas(2));
// console.log(fortuner.wheels);
// Bike.promosi()
// poligon.gowes(2)
// Bike.gowes(2)

// console.log(lambo.);
// console.log(lambo.ngeGas(4));
// console.log(pajero);
// console.log(lambo);

//Abstraction 

class Human {
  constructor(name, age) {
    if (this.constructor === Human) {
      throw new Error("This is an abstract class")
    }
    this.name = name
    this.age = age
  }

  makan(str) {
    console.log("Makan " + str);
  }
}

class Programer extends Human {
  constructor(name, age, PL) {
    super(name, age)
    this.PL = PL
  }
}

const hafis = new Programer("hafis", 23, "JS")
// console.log(hafis.makan("nasi"));

class Pohon {
  static age = 0
  constructor(longLife) {
    this.longLife = longLife
    // this.age = 0
  }

  tumbuh() {
    console.log(this.constructor.age++)
    // this.constructor.age++
  }

}

const apel = new Pohon(4)
// console.log(Pohon.age);
// apel.tumbuh()
// apel.tumbuh()
// console.log(Pohon.con);
const obj = {
  name: "Hafis",
  address: "Earth-4"
}
// const fungsi = (obj,p6,p7) => {
//   const {p1,p2,p3,p4,p5} = obj
// }
// const { addresss, name } = obj

// console.log(addresss);
