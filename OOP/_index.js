class Human {
  constructor(props) {
    console.log(this.constructor, "<<<")
    if (this.constructor === Human) {
      throw new Error("TIDAK BOLEH")
    }

    let { name, address } = props

    this.name = name
    this.address = address
    this.profession = this.constructor.name
  }

  work() {
    console.log(" WORKING ")
  }

  introduce() {
    console.log("HELLO my name is " + this.name)
  }
}

class Police extends Human {
  constructor(props) {
    super(props) // melakukan instantiate dari Parents
    this.rank = props.rank
  }

  work() {
    console.log(super.work)
  }
}


const Military = Base => class extends Base {
  shoot() {
    console.log(" SHOOT")
  }
}

class Army extends Military(Human) {
  constructor(props) {
    super(props)
  }

  work() {
    super.work()
    super.shoot()
  }
}

const prabowo = new Army({
  name: "Prabowo",
  address: "Indonesia"
})

prabowo.work()