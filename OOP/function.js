/**
 * Nama fungsi 
 * Parameter 
 * Procedure
 * Return Value
 */

function namaFungsi(props) {
  const name = props.name
  const age = props.age
  console.log(`Halo Nama saya ${name} umur saya ${age}`)
}


function addNumber(num1, num2) {  // f(x) = x1 + x2 => f(8,5) = 8-5
  const _return = num1 - num2
  return _return
}

const arrowFunc = (params1, params2) => {
  params1 + params2
}

// const voidFunc = namaFungsi({ name: "Hafis", age: 20 })
// const addFunc = addNumber(8, 5) // punya nilai balikan
// const b = arrowFunc(3, 4)
// console.log(b);
// console.log(voidFunc)
// console.log(addFunc)
// const a = addNumber("halo", "hai")
// console.log(a)
// namaFungsi("Andi", 21)
// namaFungsi("Fajar", 22)

// CALLBACK 

function print(parameter) {
  console.log(parameter)
}

function sayHello(user, f) {
  console.log("===== 1 =====")
  f(user)
}

// sayHello("User", function print2(parameter1) {
//   console.log("===== 2=====")
//   console.log(parameter1)
// })

// sayHello("HELLO", print)

const arr = [123, 43, 53, 44, 657, 65]


// arr.forEach(function printALl(el) {
//   console.log(el)
// })

// Class 

