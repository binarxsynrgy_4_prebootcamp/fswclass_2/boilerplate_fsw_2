let marvel_heroes = [
  {
    name: "RDJ",
    role: "Iron Man"
  },
  {
    name: "Andrew Garfield",
    role: "Spiderman"
  },
  {
    name: "cumberbatch",
    role: "Dr. Strange"
  },
]

let dc_heroes = [
  {
    role: "superman",
    power: "Fly"
  },
  {
    role: "the flash",
    power: "run fast"
  }
]

module.exports = {
  marvel_heroes,
  dc_heroes
}

