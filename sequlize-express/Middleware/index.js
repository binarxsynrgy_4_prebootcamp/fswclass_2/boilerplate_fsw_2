const authentication = require("./authentication")
const studentAuthorization = require("./StudentAuthorization")
module.exports = {
  authentication,
  studentAuthorization
}