const pool = require("../config")


class Student {

  static getStudents = async () => {
    const query = `
    select * from students s ;
    `

    const students = await pool.query(query)
    console.log(students)
    return students.rows
  }

  static getStudent = async (id) => {
    const query = `select * from students s where s.id  = ${id};`
    const student = await pool.query(query)
    console.log(student)
    return student.rows[0]
  }

  static createStudent = async (payload) => {
    const { name, dom } = payload
    const query = `INSERT INTO students
    ("name", "dom")
    VALUES('${name}', '${dom}')
    RETURNING *;
    `

    const student = await pool.query(query)
    console.log(student)
    return student.rows[0]
  }

  static deleteStudent = async (id) => {
    const query = `
    DELETE FROM students
    WHERE id=${id}
    RETURNING *;
    `
    const deleted = await pool.query(query)
    return deleted.rows[0]
  }

  static updateStudent = async (payload, id) => {
    let query = `
    UPDATE students SET
    `
    const arr = Object.keys(payload)
    for (let i = 0; i < arr.length; i++) {
      if (i === arr.length - 1) {
        query += `${arr[i]}= '${payload[arr[i]]}'`
      } else {
        query += `${arr[i]}= '${payload[arr[i]]}',`
      }
    }
    query += `WHERE id=${id} RETURNING * ;`
    console.log(query)
    const updated = await pool.query(query)
    return updated.rows[0]
  }


}

module.exports = Student