const route = require("express").Router()
const studentRoute = require('./student.route')


route.use("/student", studentRoute)


module.exports = route