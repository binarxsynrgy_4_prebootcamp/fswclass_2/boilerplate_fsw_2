const { StudentController } = require("../controller")
const route = require("express").Router()


route.get("/", StudentController.getStudents)
route.get("/:id", StudentController.getStudent)
route.post("/", StudentController.createStudent)
route.delete("/:id", StudentController.deleteStudent)
route.put("/:id", StudentController.updateStudent)

module.exports = route