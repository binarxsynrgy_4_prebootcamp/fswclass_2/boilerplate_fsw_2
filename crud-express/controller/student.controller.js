const { Student } = require("../models")

class StudentController {
  static async getStudents(req, res) {
    const data = await Student.getStudents()
    res.status(200).json({ data })
  }

  static async getStudent(req, res) {
    const id = req.params.id
    const data = await Student.getStudent(id)
    res.status(200).json({ data })
  }

  static async createStudent(req, res) {
    const { name, dom } = req.body
    const payload = {
      name, dom
    }
    const newStudent = await Student.createStudent(payload)
    res.status(201).json({ data: newStudent })
  }

  static async updateStudent(req, res) {
    const id_student = req.params.id
    let payload = {}
    for (let key in req.body) {
      payload[key] = req.body[key]
    }
    const updated = await Student.updateStudent(payload, id_student)
    res.status(201).json({ data: updated })

  }

  static async deleteStudent(req, res) {
    const { id } = req.params
    const deleted = await Student.deleteStudent(id)
    res.status(200).json({ data: deleted })
  }
}

module.exports = StudentController