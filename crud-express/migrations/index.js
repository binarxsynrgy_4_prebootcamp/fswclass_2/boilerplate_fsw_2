const pool = require("../config")



const migrations = async () => {
  const query_create_students = `create table IF NOT EXIST Students (
    id serial primary key,
    name varchar(255) not null ,
    dom varchar(255)
    
  );`

  const query_create_vehicles = `
  CREATE TABLE IF NOT EXIST Vehicles (
    id serial primary key, 
    type varchar(255),
    student_id int not null,
    constraint fk_vehicles_for_students foreign key(student_id) references Students(id) 
  )
  `

  try {
    await pool.query(query_create_students)
    await pool.query(query_create_vehicles)

  } catch (error) {
    console.log(error)
  }

}

migrations()

