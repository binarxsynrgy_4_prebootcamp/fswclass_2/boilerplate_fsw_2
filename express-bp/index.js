const express = require('express')
const app = express()
const port = 3000
// const router = require("./routes/index.js")
// const morgan = require("morgan")

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get('/', (req, res) => {
  console.log("======")
  console.log(req)
  if (0) {
    res.send('Hello World!')
    return
  }
  res.send("Hello AGain")

})
app.get("/users", (req, res) => {
  res.send("INI USERS")
})

// Middleware
app.use((req, res, next) => {
  console.log("===== MIddleware ======")

  const { pass } = req.body
  if (pass === "12345678") {
    next()
  } else {
    res.send("GAK BERHAK")
  }
})

//Middleware #2

// app.post("/", (req, res, next) => {
//   const { pass } = req.body
//   if (pass === "12345678") {
//     next()
//   } else {
//     res.send("GAK BERHAK")
//   }
// }, (req, res) => {

//   res.send("HELLO INI POST /")
// })
app.post("/users", (req, res, next) => {
  try {
    const { n1, n2 } = req.body
    const result = +n1 / +n2
    res.send(`${result}`)
    return

  } catch (error) {
    console.log("=======")
    next(error)
  }

})

app.delete("/users", (req, res) => {
  res.send("Delete data")
})

app.use((err, req, res, next) => {
  // if (err) {?
  console.log("===== error handler ====")
  console.log(err)
  if (err.status) {
    res.status(500).json({ status: err.status, error: err.msg })
  } else {
    res.status(500).json({ status: 500, error: err })
  }
  // }
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})


// // view Engine
// app.set("view engine", "ejs")
// // Middleware
// app.use(express.json())
// app.use(express.urlencoded({ extended: true }))
// // 3rd Party Middleware
// app.use(morgan("dev"))
// // Handler
// app.use(router)

// app.get("/home", (req, res) => {
//   res.render("index", { name: "hafis" })
// })

// //error handler
// app.use((err, req, res, next) => {
//   console.log(err)
//   res.status(500).json({ status: 500, error: err })
// })

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })