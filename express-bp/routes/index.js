const express = require("express")
const router = express.Router()




router.get('/user', (req, res, next) => {
  //disini logic handler
  try {

    const { n1, n2 } = req.body
    res.send(+n1 / +n2)
  } catch (error) {
    console.log(error)
    next(error)

  }
})


// Custom middleware
router.use((req, res, next) => {
  try {
    console.log("MIDDLEWARE")
    const { name, pass } = req.body
    console.log(name, pass)
    if (name === "hafis" && pass === "12345678") {
      next()
    } else {
      throw {
        status: 401,
        msg: "GK BOLEH"
      }
      // return
    }
  } catch (error) {
    next(error)
  }

})
router.get("/test", (req, res, next) => {
  res.send(" INI TEST")
})


router.post("/user", (req, res, next) => {
  res.send("ini method post")
})


router.delete("/user", (req, res, next) => {
  res.send("INI DELETE USER")
})

module.exports = router
