const http = require("http")
const fs = require('fs')

const onRequest = (req, res) => {
  const header = {}
  res.writeHead(200, { "Content-Type": "text/html" })
  fs.readFile("./index.html", null, function (err, data) {
    if (err) {
      res.writeHead(401, { "Content-Type": "text/html" })
      const data = fs.readFileSync("./notfound.html", null)
      res.write(data)
    } else {

      res.write(data)
    }
    res.end()
  })
  console.log("hello")
}
http.get("/")

const jsonRequest = (req, res) => {
  res.writeHead(200, { "Content-Type": "application/x-www-form-urlencoded" })
  console.log(req)

  const data = {
    name: 'RDJ',
    Role: "Iron man"
  }

  res.end(JSON.stringify(data))
}



http.createServer(onRequest).listen(443, () => {
  console.log(" sukses")
})


